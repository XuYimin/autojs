auto();
device.keepScreenOn(3600*1000);
setScreenMetrics(1080, 2244);

// 请求截图
if(!requestScreenCapture()) {
    toast("请求截图失败");
    exit();
}

function 已收藏() {
    var img = captureScreen();
    var color = images.pixel(img, 846, 2074);
    log(color);
    if(colors.isSimilar("#ffffc740", colors.toString(color))) {
        // toast("已收藏");
        return true;
    } else {
        // toast("未收藏");
        return false;
    }
}

function 左上角返回() {
    click(100, 180);
    sleep(500);
}

function 下一篇() {
    左上角返回();
    // 向下滑动
    swipe(100,1000,100,500,600);
    sleep(1000);   
    
     // 进入
    click(500, 750);
    sleep(1000);
}

function 发表评论() {
    click("欢迎发表你的观点", 0);
    sleep(1000);
    className("android.widget.EditText").findOne().setText("学习强国，为建设伟大祖国不懈奋斗！");
    sleep(1000);
    click("发布",0);
    sleep(1000);
}

function 删除评论() {
    click("删除", 0);
    sleep(1000);
    click("确认", 0);
    sleep(1000);
}

function 评论() {
    发表评论();
    删除评论();
}

function 阅读() {
    var 收藏按钮 = bounds(774,2038,918,2110).findOne(2000);
    if(收藏按钮 != null ) {
        if(已收藏()) {
            左上角返回();
            下一篇();
            return false;
        }
        else{
            评论();
            收藏按钮.click();
            sleep(2000);
            return true;
        }
    }
    else{
        左上角返回();
        下一篇();
        return false;
    }
}



function 批量评论(n){
    var i = 0;
    while (i < n)
    {
        if(阅读()) {
            i += 1;
            toast(i);
        }
    }
}

批量评论(11);