auto();
device.keepScreenOn(15 * 60 * 1000);

function 返回() {
    click(100, 180);
    sleep(1000);
}

function 学习频道(频道) {
    click(1038, 303); // 扩展栏
    sleep(1000);

    click(频道, 0);
    sleep(2000);
}

function 打开文章() {
    bb = text("播报").findOne(); 
    var b = bb.bounds();
    click(500, b.centerY());
    sleep(1000);
}

function 滑动(n) {
    for(i = 0; i < n*20; i++) {
        swipe(100, 1000, 100, 500, 600);
        sleep(3000);
        toast(i + 1 + "/" + n*20);
    }
}

function getNhd() { // 滑动分钟数
    var storage = storages.create("xxqg");
    nhd = 1 * storage.get("nhd");
    toast("滑动分钟： " + nhd);
    return nhd;
}

toast("滑动 开始");
nhd = getNhd();
学习频道("要闻");
打开文章();
滑动(nhd);
返回();
toast("滑动 结束");
sleep(1000);