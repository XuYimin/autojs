auto();
if(!requestScreenCapture()) { // 请求截图
    toast("请求截图失败");
    exit();
}

function 返回() {
    click(100, 180);
    sleep(1000);
}

function 已收藏() {
    var img = captureScreen();
    var color = images.pixel(img, 858, 2074);
    log(color);
    if(colors.isSimilar("#ffffc740", colors.toString(color))) {
        return true;
    } else {
        return false;
    }
}

function 发表评论() {
    sleep(1000);
    click("欢迎发表你的观点", 0);
    sleep(1000);
    className("EditText").findOne().setText("学习强国，为建设伟大祖国不懈奋斗！");
    sleep(1000);
    click("发布",0);
    sleep(1000);
    click("删除", 0);
    sleep(1000);
    click("确认", 0);
    sleep(1000);
}

var storage = storages.create("xxqg");
ydhd = storage.get("ydhd");
ydhd = false;
function 滑动() {
    if(ydhd) {
        for(i = 0; i < 20; i++) {
            swipe(100, 1000, 100, 500, 600);
            sleep(3200);
            toast(i + 1 + "/" + n*20);
        }
    }
}

var pl = {};
pl.r = function 评论返回() {
    var 收藏按钮 = bounds(792, 2038, 924, 2110).findOne(2000);
    if(收藏按钮 != null ) {
        if(已收藏()) {
            返回();
            return false;
        } else {
            发表评论();
            滑动();
            收藏按钮.click();
            sleep(3000);
            返回();
            return true;
        }
    } else {
        返回();
        return false;
    }
}
module.exports = pl;