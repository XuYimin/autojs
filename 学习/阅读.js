auto();
device.keepScreenOn(10 * 60 * 1000);

var pl = require(files.getSdcardPath() + "/脚本/学习/M_评论.js");

function 返回() {
    click(100, 180);
    sleep(1000);
}

function 翻屏() {
    swipe(100, 1928, 100, 390, 1000);
    sleep(1000);
}

function 学习频道(频道) {
    click(1038, 303); // 扩展栏
    sleep(1000);

    click(频道, 0);
    sleep(2000);
}

function getN() {
    var storage = storages.create("xxqg");
    nyd = 1 * storage.get("nyd");
    toast("阅读数量： " + nyd);
    sleep(1000);
    return nyd;
}

function 学习() {
    n = getN();

    i = 0;
    while(true) {        
        bbs = text("播报").boundsInside(852, 400, 1026, 1950).find();

        try {
            bbs.forEach((element) => {
                var b = element.bounds();
                click(500, b.centerY());
                sleep(2000);

                if(pl.r()) {
                    i++;
                    toast(i + " / " + n);
                }

                if(i >= n) {
                    throw new Error("break");
                    return;
                }
            });
        } catch (e) {
            toast("阅读 结束");
            return;
        }

        翻屏();
    }
};

var storage = storages.create("xxqg");
storage.put("ydhd", true);
toast("阅读 开始");
学习频道("推荐");
学习();
toast("阅读 结束");
storage.put("ydhd", false);
sleep(1000);