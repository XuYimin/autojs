auto();

function unlock() {
    if (!device.isScreenOn()) {
        device.wakeUp();
        sleep(1000);

        swipe(500, 1500, 500, 1000, 300); // 滑动解锁
        sleep(1000);
    }

    home();
    sleep(1000);
}

unlock();
device.keepScreenOn(30 * 60 * 1000);

launchApp("学习强国");
sleep(30000);
require(files.getSdcardPath() + "/脚本/学习.js");