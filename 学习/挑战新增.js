"ui";

ui.layout(
    <vertical padding="16">
        <button id="tc" text="退出"/>
        <text id="q"></text>
        <radiogroup>
            <radio id="a1"/>
            <radio id="a2"/>
            <radio id="a3"/>
            <radio id="a4"/>
        </radiogroup>
        <button id="tzxz" text="挑战新增"/>

    </vertical>
);

var storage = storages.create("xxqg");

importClass(android.database.sqlite.SQLiteDatabase);
var sdpath = files.getSdcardPath();
var dbName = "/脚本/学习/xxqg.db";
var path = files.path(sdpath + dbName);
if(!files.exists(path)) {
    files.createWithDirs(path);
}

function 题目() {
    ui.q.setText(storage.get("q", "q"));
}

function 选项() {
    ui.a1.setText(storage.get("a1", "a1"));
    ui.a2.setText(storage.get("a2", "a2"));
    ui.a3.setText(storage.get("a3", "a3"));
    ui.a4.setText(storage.get("a4", "a4"));
};

function 单选删除(q) {
    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var sql = "DELETE FROM dx WHERE q='" + q + "'";
    log(sql);
    db.execSQL(sql);
    db.close();
    toast("delete");
}

function 单选新增(q, a) {
    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var sql = "INSERT INTO dx (q,a,bz) values('" + q + "','" + a + "','tz')";
    log(sql);
    db.execSQL(sql);
    db.close();
    toast("insert");
}

ui.tc.on("click", ()=>{
    exit();
});

function 选中项() {
    if(ui.a1.checked) {
        return ui.a1.text;
    }
    if(ui.a2.checked) {
        return ui.a2.text;
    }
    if(ui.a3.checked) {
        return ui.a3.text;
    }
    if(ui.a4.checked) {
        return ui.a4.text;
    }
}

ui.tzxz.on("click", ()=>{
    q = storage.get("q", "q");
    a = 选中项();
    单选删除(q);
    单选新增(q, a);
    exit();
});

function main() {
    题目();
    选项();
}

main();