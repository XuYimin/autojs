auto();
var sdpath = files.getSdcardPath();

function main() {
    var storage = storages.create("xxqg");

    if (storage.get("cyd")) {
        require(sdpath + "/脚本/学习/阅读.js");
    }
    
    if (storage.get("csp")) {
        require(sdpath + "/脚本/学习/视频.js");
    }

    if (storage.get("cbdpd")) {
        require(sdpath + "/脚本/学习/本地频道.js");
    }

    if (storage.get("cfx")) {
        require(sdpath + "/脚本/学习/分享.js");
    }

    if (storage.get("chd")) {
        require(sdpath + "/脚本/学习/滑动.js");
    }
    
    if (storage.get("cxwlb")) {
        require(sdpath + "/脚本/学习/新闻联播.js");
    }
}

device.setMusicVolume(0);
main();