auto();

if(!requestScreenCapture()) { // 请求截图
    toast("请求截图失败");
    exit();
}

var storage = storages.create("xxqg");

importClass(android.database.sqlite.SQLiteDatabase);
var sdpath = files.getSdcardPath();
var dbName = "/脚本/学习/xxqg.db";
var path = files.path(sdpath + dbName);
if(!files.exists(path)) {
    files.createWithDirs(path);
}

var window = floaty.window(
    <vertical>
            <horizontal>
                <checkbox id="dj" checked="false" text=""/>
                <checkbox id="xz" checked="false" text=""/>
                {/* <button id="add" text="新增" w="40" h="36" bg="#77ffffff"/> */}
            </horizontal>
        <button id="action" text="挑战" w="80" h="36" bg="#77ffffff"/>
    </vertical>
);

setInterval(()=>{}, 1000);

var x = 0, y = 0;       // 记录按键被按下时的触摸坐标
var windowX, windowY;   // 记录按键被按下时的悬浮窗位置
var downTime;           // 记录按键被按下的时间以便判断长按等动作

window.action.setOnTouchListener(function(view, event) {
    switch(event.getAction()) {
        case event.ACTION_DOWN:
            x = event.getRawX();
            y = event.getRawY();
            windowX = window.getX();
            windowY = window.getY();
            downTime = new Date().getTime();
            return true;

        case event.ACTION_MOVE:
            // 移动手指时调整悬浮窗位置
            window.setPosition(windowX + (event.getRawX() - x),  windowY + (event.getRawY() - y));
            return true;

        case event.ACTION_UP:
            // 如果按下的时间超过 3 秒判断为长按，删除内容
            if(new Date().getTime() - downTime > 3000) {
                onDelelte();
            }
            // 手指弹起时如果偏移很小则判断为点击
            if(Math.abs(event.getRawY() - y) < 5 && Math.abs(event.getRawX() - x) < 5) {
                main();
            }
            return true;
    }
    return true;
});

function 题目() {
    var t = textMatches(/.+/).boundsContains(114, 473, 114, 473).findOne(500);
    if(t == null) {
        toast("题目 超时");
        return null;
    } else {
        var q = t.text();
        return q;
    }
}

function 选项() {
    xx = textMatches(/.+/).clickable(true).find();
    storage.put("a1", "");
    storage.put("a2", "");
    storage.put("a3", "");
    storage.put("a4", "");
    
    var i = 1;
    xx.forEach((element) => {
        a = element.text(); // 答案
        switch(i) {
            case 1:
                storage.put("a1", a);
                break;            
            case 2:
                storage.put("a2", a);
                break;
            case 3:
                storage.put("a3", a);
                break;            
            case 4:
                storage.put("a4", a);
                break;                
        }
        i++;
    });
    return xx;
};

function 点选() {
    xx = 选项();
    cnt = xx.length;
    if(cnt == 0) {
        return false;
    }

    var i = random(0, cnt - 1);
    d = xx[i].text();
    sx = text(d).findOne();
    sx.click();
    return true;
}

function 单选查询(q) {
    window.xz.checked = true; // 新增设置
    query = "SELECT a FROM dx WHERE q='" + q + "'";
    log(query);
    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var cursor = db.rawQuery(query, null);
    if(cursor.getCount() > 0) {
        window.xz.checked = false;
        cursor.moveToNext();
        var a = cursor.getString(0);
        toast(a);

        xx = text(a).findOne(1000); // 选项
        if(xx == null) {
            toast(a + " 未找到");
        } else {
            xx.click();
        }
        
        db.close();
        return true;
    }
    db.close();
    toast("null");
    return false;
}

function main() {
    var q = 题目();
    if(q == null) {
        return;
    }
    storage.put("q", q);

    if(单选查询(q)) {
        return;
    }

    if(window.dj.checked) {
        if(!点选()) {
            return;
        }
    }

    xx = 选项();
    setTimeout(function() {
        var img = captureScreen();
        img.saveTo(sdpath + '/脚本/学习/000.jpg');
    
        xx.forEach((element) => {
            var b = element.bounds();
            var color = images.pixel(img, b.right + 10, b.centerY()); // 选项超出屏幕
            if(colors.isSimilar("#ff3ebd73", colors.toString(color))) {
                var db = SQLiteDatabase.openOrCreateDatabase(path, null);
                a = element.text(); // 答案
                var sql = "INSERT INTO dx (q,a,bz) values('" + q + "','" + a + "','tz')";
                log(sql);
                db.execSQL(sql);
                db.close();
                toast("insert");
            }
        });
    }, 150);
}

function 重启() {
    x = desc("结束本局").findOne(500);
    if(x == null) {
        return;
    }
    x.click();

    x = desc("挑战答题").findOne(500);
    if(x == null) {
        return;
    }
    x.click();
}

window.xz.on("touch_up", ()=>{
    engines.execScriptFile(sdpath + "/脚本/学习/挑战新增.js");
});