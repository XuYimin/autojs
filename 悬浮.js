auto();

importClass(android.view.View); // setVisibility

if(!requestScreenCapture()) { // 请求截图
    toast("请求截图失败");
    exit();
}

importClass(android.database.sqlite.SQLiteDatabase);
var sdpath = files.getSdcardPath();
var dbName = "/脚本/学习/xxqg.db";
var path = files.path(sdpath + dbName);
if(!files.exists(path)) {
    files.createWithDirs(path);
}

var window = floaty.window(
    <vertical>
        <input id="input" text="" textSize="16sp"/>
        <button id="action" text="开始运行" w="100" h="36" bg="#77ffffff"/>
    </vertical>
);

setInterval(()=>{}, 1000);

var x = 0, y = 0;       // 记录按键被按下时的触摸坐标
var windowX, windowY;   // 记录按键被按下时的悬浮窗位置
var downTime;           // 记录按键被按下的时间以便判断长按等动作

window.input.on("touch_down", ()=>{
    downTime = new Date().getTime();
    window.requestFocus();
    window.input.requestFocus();
    window.action.setVisibility(View.GONE);
});

window.input.on("touch_up", ()=>{
    if(new Date().getTime() - downTime > 500) { // 按下时间超过 0.5 秒，失去焦点
        window.disableFocus();
        window.action.setVisibility(View.VISIBLE);
    }
});

window.action.setOnTouchListener(function(view, event) {
    switch(event.getAction()) {
        case event.ACTION_DOWN:
            x = event.getRawX();
            y = event.getRawY();
            windowX = window.getX();
            windowY = window.getY();
            downTime = new Date().getTime();
            return true;

        case event.ACTION_MOVE:
            // 移动手指时调整悬浮窗位置
            window.setPosition(windowX + (event.getRawX() - x),  windowY + (event.getRawY() - y));
            return true;

        case event.ACTION_UP:
            // 如果按下的时间超过 3 秒判断为长按，删除内容
            if(new Date().getTime() - downTime > 3000) {
                onDelelte();
            }
            // 手指弹起时如果偏移很小则判断为点击
            if(Math.abs(event.getRawY() - y) < 5 && Math.abs(event.getRawX() - x) < 5) {
                onClick();
            }
            return true;
    }
    return true;
});

function 题型() {
    var t = textContains("题").boundsInside(54, 305, 1026, 458).findOne(1000);
    if(t == null) {
        toast("题型 超时");
        return null;
    } else {
        var d = t.text().substring(0,3);
        // toast(d);
        return d;
    }
}

function 题目() {
    var t = textMatches(/.+/).boundsContains(213, 579, 213, 579).findOne(1000);
    if(t == null) {
        toast("题目 超时");
        return null;
    } else {
        var q = t.text();
        // toast(q);
        return q;
    }
}

function 填空题目() {
    q = 题目();
    if (q == null) {
        return null;
    }

    // 短填空添加辅助字段 q1
    if (q.length <= 20) {
        var t = textMatches(/.*/).boundsContains(800, 610, 800, 610).findOne();
        var q1 = t.desc();
        return {q:q, q1:q1};
    } else {
        return {q:q, q1:null};
    }
}

function onDelelte() {
    log("长按");
    
    var d = 题型();
    if(d == null) {
        return;
    }

    var q = 题目();
    if(q == null) {
        return;
    }

    var tbl = "";   // 获取表名
    switch(d) {
        case "填空题":
            tbl = "tk";
            break;
        case "单选题":
            tbl = "dx";
            break;
        case "多选题":
            tbl = "dxs";
            break;
    }

    if (tbl == "") {
        return;
    } else {
        var db = SQLiteDatabase.openOrCreateDatabase(path, null);
        var sql = "DELETE FROM " + tbl + " WHERE q='" + q + "'";
        log(sql);
        db.execSQL(sql);
        db.close();
        toast("delete");
    }
}

function onClick() {
    var d = 题型();
    if(d == null) {
        return;
    }

    window.input.setVisibility(View.GONE);

    if(d == "填空题") {
        if (window.action.getText() == "填空新增") {
            填空新增();
        } 
        window.action.setText("填空");
        填空查询();
    }

    if(d == "单选题") {
        if (window.action.getText() == "单选新增") {
            单选新增();
        } 
        window.action.setText("单选");
        单选查询();
    }

    if(d == "多选题") {
        var bl = boundsInside(54,311,1026,2140).boundsContains(54,344,1026,2140).findOne(2000); // bounds long 超出屏幕提示
        if (bl != null) {
            toast("超出屏幕");
            return;
        }

        if (window.action.getText() == "多选新增") {
            多选新增();
        } 
        window.action.setText("多选");
        多选查询();
    }

    // toast("click");
}

function 填空查询() {
    var tkq = 填空题目();
    if(tkq == null) {
        return;
    }

    q = tkq.q;
    q1 = tkq.q1;
    if(q1 == null) {
        query = "SELECT a FROM tk WHERE q='" + q + "'";
    } else {
        query = "SELECT a FROM tk WHERE q='" + q + "' AND q1='" + q1 + "'";
    }
    log(query);

    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var cursor = db.rawQuery(query, null);
    if(cursor.getCount() > 0) {
        cursor.moveToNext();
        var a = cursor.getString(0);
        var ar = a.split("|");
        for(var i = 0; i < ar.length; i++) {
            setText(i, ar[i]);
        }
        window.input.setVisibility(View.GONE);
    } else {
        window.action.setText("填空新增");
        window.input.setVisibility(View.VISIBLE);
    }
    db.close();
}

function 填空新增() {
    var tkq = 填空题目();
    if(tkq == null) {
        return;
    }

    var a = window.input.text();    // 解答
    log(a);
    if(a.length == 0) {
        return;
    }

    q = tkq.q;
    q1 = tkq.q1;
    if(q1 == null) {
        var sql = "INSERT INTO tk (q,a) values('" + q + "','" + a + "')";
    } else {
        var sql = "INSERT INTO tk (q,q1,a) values('" + q + "','" + q1 + "','" + a + "')";
    }
    log(sql);

    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    db.execSQL(sql);
    db.close();
    toast("insert");

    window.input.setText("");
    window.action.setText("填空");
}

function 单选查询() {
    var q = 题目();
    if(q == null) {
        return;
    }

    query = "SELECT a FROM dx WHERE q='" + q + "'";
    log(query);
    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var cursor = db.rawQuery(query, null);
    if(cursor.getCount() > 0) {
        cursor.moveToNext();
        var a = cursor.getString(0);
        toast(a);
        xx = text(a).findOne(1000); // 选项
        if(xx == null) {
            toast(a + " 未找到");
            window.action.setText("单选新增");
        } else {
            xx.click();
        }
    } else {
        window.action.setText("单选新增");
    }
    db.close();
}

function 单选新增() {
    var q = 题目();
    if(q == null) {
        return;
    }

    var img = captureScreen();
    xx = textMatches(/.+/).boundsInside(243, 530, 927, 2140).find(); // 选项

    xx.forEach((element) => {
        var b = element.bounds();
        var color = images.pixel(img, b.right, Math.min(b.bottom, 2139)); // 选项超出屏幕
        if(colors.isSimilar("#fff8f7f2", colors.toString(color))) {
            a = element.text(); // 答案
            var sql = "INSERT INTO dx (q,a) values('" + q + "','" + a + "')";
            log(sql);
            var db = SQLiteDatabase.openOrCreateDatabase(path, null);
            db.execSQL(sql);
            db.close();
            toast("insert");
            window.action.setText("单选");
        } 
    });
}

function 多选查询() {
    var q = 题目();
    if(q == null) {
        return;
    }

    query = "SELECT a FROM dxs WHERE q='" + q + "'";
    log(query);
    var db = SQLiteDatabase.openOrCreateDatabase(path, null);
    var cursor = db.rawQuery(query, null);
    if(cursor.getCount() > 0) {
        多选清空();
        while(cursor.moveToNext()) {
            var a = cursor.getString(0);
            toast(a);
            xx = text(a).findOne(); // 选项
            xx.click();
        }
    } else {
        window.action.setText("多选新增");
    }
    db.close();

    // toast("click");
}

function 多选新增() {
    var q = 题目();
    if(q == null) {
        return;
    }

    var img = captureScreen();
    xx = textMatches(/.+/).boundsInside(243, 530, 927, 2140).find(); // 选项

    xx.forEach((element) => {
        var b = element.bounds();
        var color = images.pixel(img, b.right, Math.min(b.bottom, 2139)); // 选项超出屏幕
        if(colors.isSimilar("#fff8f7f2", colors.toString(color))) {
            if(b.left == 243 && b.right == 927) { // 选项左右边界
                a = element.text(); // 答案
                var sql = "INSERT INTO dxs (q,a) values('" + q + "','" + a + "')";
                log(sql);
                var db = SQLiteDatabase.openOrCreateDatabase(path, null);
                db.execSQL(sql);
                db.close();
                toast("insert");
            }
        } 
    });
    window.action.setText("多选");
}

function 多选清空() {
    var img = captureScreen();
    xx = textMatches(/.+/).boundsInside(243, 530, 927, 2140).find(); // 选项

    xx.forEach((element) => {
        var b = element.bounds();
        var color = images.pixel(img, b.right, Math.min(b.bottom, 2139)); // 选项超出屏幕
        if(colors.isSimilar("#fff8f7f2", colors.toString(color))) {
            if(b.left == 243 && b.right == 927) { // 选项左右边界
                xx = text(element.text()).findOne();
                xx.click();
            }
        } 
    });
}