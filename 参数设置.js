"ui";

ui.layout(
    <vertical padding="16">
        <horizontal>
            <checkbox id="yd" checked="true" text="阅读数量"/>
            <input inputType="number" id="nyd" text="12"/>
        </horizontal>
        <horizontal>
            <checkbox id="sp" checked="true" text="视频数量"/>
            <input inputType="number" id="nsp" text="12"/>
        </horizontal>
        <checkbox id="bdpd" checked="true" text="本地频道"/>
        <checkbox id="fx" checked="true" text="分享"/>
        <horizontal>
            <checkbox id="hd" checked="true" text="滑动分钟"/>
            <input inputType="number" id="nhd" text="12"/>
        </horizontal>
        <checkbox id="xwlb" checked="true" text="新闻联播"/>
        <button id="hl" text="耗流"/>         
        <button id="ql" text="轻流"/> 
        <button id="qx" text="全选"/>     
        <button id="cssz" text="参数设置"/>   
    </vertical>
);

function 加载变量() {    
    var storage = storages.create("xxqg");
    ui.yd.checked = storage.get("cyd", true);       // 阅读
    ui.nyd.setText(storage.get("nyd", 12));         // 阅读数量
    ui.sp.checked = storage.get("csp", true);       // 视频
    ui.nsp.setText(storage.get("nsp", 12));         // 视频数量
    ui.bdpd.checked = storage.get("cbdpd", true);   // 本地频道
    ui.fx.checked = storage.get("cfx", true);       // 分享
    ui.hd.checked = storage.get("chd", true);       // 滑动
    ui.nhd.setText(storage.get("nhd", 12));         // 滑动分钟
    ui.xwlb.checked = storage.get("cxwlb", true);   // 新闻联播
}

function 存储变量() {
    var storage = storages.create("xxqg");
    storage.put("cyd", ui.yd.checked);      // 阅读
    storage.put("nyd", ui.nyd.text());      // 阅读数量
    storage.put("csp", ui.sp.checked);      // 视频
    storage.put("nsp", ui.nsp.text());      // 视频数量
    storage.put("cbdpd", ui.bdpd.checked);  // 本地频道
    storage.put("cfx", ui.fx.checked);      // 分享
    storage.put("chd", ui.hd.checked);      // 滑动
    storage.put("nhd", ui.nhd.text());      // 滑动分钟
    storage.put("cxwlb", ui.xwlb.checked);  // 新闻联播
    toast("存储成功");
}

加载变量();

ui.hl.on("click", ()=>{
    ui.yd.checked = false;  // 阅读
    ui.sp.checked = true;   // 视频
    ui.bdpd.checked = true; // 本地频道
    ui.fx.checked = false;  // 分享
    ui.hd.checked = false;  // 滑动
    ui.xwlb.checked = true; // 新闻联播
});

ui.ql.on("click", ()=>{
    ui.yd.checked = true;       // 阅读
    ui.sp.checked = false;      // 视频
    ui.bdpd.checked = false;    // 本地频道
    ui.fx.checked = true;       // 分享
    ui.hd.checked = true;       // 滑动
    ui.xwlb.checked = false;    // 新闻联播
});

ui.qx.on("click", ()=>{
    ui.yd.checked = true;   // 阅读
    ui.sp.checked = true;   // 视频
    ui.bdpd.checked = true; // 本地频道
    ui.fx.checked = true;   // 分享
    ui.hd.checked = true;   // 滑动
    ui.xwlb.checked = true; // 新闻联播
});

ui.cssz.on("click", ()=>{
    存储变量();
    exit();
});